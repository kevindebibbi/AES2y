clc, clear, close all

%% DATI
m2 = 250;           %[kg/h] portata massica
p1 = 101325;        %[Pa] P1 = P4 = P5 = P6
p4 = p1; p5 = p1; p6 = p1;
B21 = 3.2;          % P2/P1
B32 = 0.95;         % P3/P2
B71 = 0.95;         % P7/P1
T4 = 5+273.15;      %[K] temperatura 4
gamma = 1.4;        % Cp/Cv coefficiente della politropica
gamma_A = 1.05;     % strozzatura A
gamma_B = 1.37;     % strozzatura B
eff = 0.8;          % Efficienza scambiatore
n_comp = 0.9;       % Rendimento Compressore
n_turb = 0.9;       % Rendimento Turbina

%% CASO T1 = 10°C, T6 = 24°C
T1 = 10 + 273.15;                                       %[K] Temperatura 1
%STATO 2
p2 = B21 * p1;                                          %[Pa] Pressione 2
T2_is = (B21)^((gamma-1)/gamma)*T1;                     %[K] Temperatura 2 isoentropica
T2 = T1+(T2_is-T1)/n_comp;                              %[K] Temperatura 2 reale
%STATO 3
p3 = p2 * B32;                                          %[Pa] pressione 3
T3_is = (p4/p3)^((1-gamma)/gamma)*T4;                   %[K] Temperatura 3 isoentropica
T3 = T4/(1-n_turb+n_turb*(p4/p3)^((gamma-1)/gamma));    %[K] Temperatura 3 reale
%STATO 5 - Valvola A
T5 = T2*(p5/p2)^((gamma_A-1)/gamma_A);                  %[K] Temperatura 5
%STATO 6 
T6 = 24+273.15;                                         %[K] 
%STATO 7 - Valvola B
p7 = B71*p1;                                            %[Pa] Pressione 7
T7 = T1*(p7/p1)^((gamma_B-1)/gamma_B);                  %[K] Temperatura 7
%PORTATA MASSICA 
m5 = m2*((T6-T4)/(T5-T4));
m4 = m2 - m5;
m7 = m4*(T2-T3)/(eff*(T2-T7));
%STATO 8
T8 = (T2-T3)*(m4/m7)+T7;

fprintf('* CASO 1: T1 = 10°C, T6 = 24°C\n')
fprintf('-STATO 1\nT1 = %f[K],\tp1 = %g[Pa]\n',T1,p1)
fprintf('-STATO 2\nT2 = %f[K],\tp2 = %g[Pa]\n',T2,p2)
fprintf('-STATO 3\nT3 = %f[K],\tp3 = %g[Pa]\n',T3,p3)
fprintf('-STATO 4\nT4 = %f[K],\tp4 = %g[Pa]\n',T4,p4)
fprintf('-STATO 5\nT5 = %f[K],\tp5 = %g[Pa]\n',T5,p5)
fprintf('-STATO 6\nT6 = %f[K],\tp6 = %g[Pa]\n',T6,p6)
fprintf('-STATO 7\nT7 = %f[K],\tp7 = %g[Pa]\n',T7,p7)
fprintf('-STATO 8\nT8 = %f[K],\tp8 = ~\n',T8)
fprintf('M4 = %g, M5 = %g, M7 = %g\n',m4,m5,m7)

%% CASO T1 = 34°C, T6 = 22°C
T1 = 34 + 273.15;                                       %[K] Temperatura 1
%STATO 2
p2 = B21 * p1;                                          %[Pa] Pressione 2
T2_is = (B21)^((gamma-1)/gamma)*T1;                     %[K] Temperatura 2 isoentropica
T2 = T1+(T2_is-T1)/n_comp;                              %[K] Temperatura 2 reale
%STATO 3
p3 = p2 * B32;                                          %[Pa] pressione 3
T3_is = (p4/p3)^((1-gamma)/gamma)*T4;                   %[K] Temperatura 3 isoentropica
T3 = T4/(1-n_turb+n_turb*(p4/p3)^((gamma-1)/gamma));    %[K] Temperatura 3 reale
%STATO 5 - Valvola A
T5 = T2*(p5/p2)^((gamma_A-1)/gamma_A);                  %[K] Temperatura 5
%STATO 6 
T6 = 22+273.15;                                         %[K] 
%STATO 7 - Valvola B
p7 = B71*p1;                                            %[Pa] Pressione 7
T7 = T1*(p7/p1)^((gamma_B-1)/gamma_B);                  %[K] Temperatura 7
%PORTATA MASSICA 
m5 = m2*((T6-T4)/(T5-T4));
m4 = m2 - m5;
m7 = m4*(T2-T3)/(eff*(T2-T7));
%STATO 8
T8 = (T2-T3)*(m4/m7)+T7;

fprintf('\n\n* CASO 2: T1 = 10°C, T6 = 24°C\n')
fprintf('-STATO 1\nT1 = %f[K],\tp1 = %g[Pa]\n',T1,p1)
fprintf('-STATO 2\nT2 = %f[K],\tp2 = %g[Pa]\n',T2,p2)
fprintf('-STATO 3\nT3 = %f[K],\tp3 = %g[Pa]\n',T3,p3)
fprintf('-STATO 4\nT4 = %f[K],\tp4 = %g[Pa]\n',T4,p4)
fprintf('-STATO 5\nT5 = %f[K],\tp5 = %g[Pa]\n',T5,p5)
fprintf('-STATO 6\nT6 = %f[K],\tp6 = %g[Pa]\n',T6,p6)
fprintf('-STATO 7\nT7 = %f[K],\tp7 = %g[Pa]\n',T7,p7)
fprintf('-STATO 8\nT8 = %f[K],\tp8 = ~\n',T8)
fprintf('M4 = %g, M5 = %g, M7 = %g\n',m4,m5,m7)








