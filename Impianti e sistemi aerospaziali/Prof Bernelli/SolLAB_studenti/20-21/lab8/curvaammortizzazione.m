clc, clear, close all

%% DATI
delta = 0.300;      %[m] schiacciamento massimo
p0 = 2.8*10^6;      %[Pa] pressione precarica azoto
V0 = 0.026;         %[m^3] volume iniziale azoto
ro = 780;           %[kg/m^3] densità dell'olio
ksi = 1.6;          %coefficiente di perdita di carico
Dc = 0.240;         %[m] diametro cilindro
Ds = 0.100;         %[m] diametro spina
Da = 0.018;         %[m] diametro singolo foro a;
n = 4;              %numero fori
Ai_max = 150*1e-6;  %sezione massima dell'intaglio nella spina
g = 1.4;        %(Cp/Cv) costante dei gas
%% (1.)CALCOLO R_gas
Ac = (pi/4) * Dc^2;                     %[m^2] area cilindro
corsa = 0.001:0.001:delta;              %[m] corsa
p_gas = p0.*(V0./(V0-Ac.*corsa)).^g;    %[Pa] pressione gas
R_gas = p_gas*Ac;                       %[N] reazione gas

%% (2.)CALCOLO R_oil
Aa = n*(pi/4) * (Da)^2;                 %[m^2] area 4 fori
As = (pi/4) * Ds^2;                     %[m^2] area spina
% AREA TRAFILAMENTO
A_tf = @(x) ((x<=0.05).* (0.*x+Aa))+...
          ((0.05<x & x<=0.07) .* (Aa+Ai_max.*(x-0.05)./(0.07-0.05)))+...
          ((0.07<x & x<=0.17) .* (Aa+Ai_max+0.*x)) +...
          ((0.17<x & x<=0.21) .* (Aa+Ai_max-Ai_max.*(x-0.17)./(0.21-0.17))) +...
          ((0.21<x & x<=0.30) .* (0.*x+Aa));    %[m^2]
%L'area trafilamento dipende dalla posizione del martinetto

%VELOCITA' MARTINETTO [m/s]
x_punto = @(x) ((x<=0.08) .* (-440.*x.^2+70.4.*x)) + ...
            ((0.08<x & x<=0.3) .* (-58.*x.^2+9.28.*x+2.44));    
%VELOCITA' TRAFILAMENTO   (Q = (Ac-As)*x_punto ; Q = A_tf * x)
v =@(x) ((Ac-As)./(A_tf(x))).*x_punto(x);
% DELTA_P [Pa]  
dP = @(x)(0.5*ksi*ro)*(v(x)).^2;

R_oil = dP(corsa)*(Ac-As);                 
R_tot = R_gas + R_oil;
R_max = max(R_tot);

R_app = polyfit(corsa,R_tot,11);   %Approssimazione polinomiale di Rtot
I = diff(polyval(polyint(R_app),[0 0.3])); %Integrale sulla corsa di Rtot
eta = I/(R_max*delta);             %Rendimento 

fprintf('\nIl rendimento vale:\t%f\n',eta)
fprintf('La R_max vale:\t%g\n',R_max)

figure(1), plot(corsa,R_oil,'r',corsa,R_gas,'b',corsa,R_tot,'g',[0 0.3],[R_max R_max],'k')
xlabel('Schiacciamento X [m]')
ylabel('Reazione R [N]')
legend('OLIO','GAS','TOTALE','R_m_a_x')
grid on

figure(2), plot(corsa,x_punto(corsa),'k')
xlabel('Schiacciamento X [m]')
ylabel('Velocità Martinetto X^. [m/s]')
grid on

figure(3), plot(corsa,v(corsa),'k')
xlabel('Schiacciamento X [m]')
ylabel('Velocità Trafilamento V [m/s]')
grid on

figure(4), plot(corsa,A_tf(corsa),'r')
xlabel('Schiacciamento X [m]')
ylabel('Area Tlafilamento [m^2]')
grid on


