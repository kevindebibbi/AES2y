function xdot=forza(x)
global G1 alfa
     xdot=zeros(4,1);
     xdot(1)=x(3);
     xdot(2)=x(4);
     ro=sqrt(x(1)^2+x(2)^2);
     xdot(3)=-G1*x(1)/ro^(alfa+1);
     xdot(4)=-G1*x(2)/ro^(alfa+1);
endfunction     
