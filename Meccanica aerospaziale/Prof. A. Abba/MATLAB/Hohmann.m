pi=atan(1.0)*4;
mt=5.97219e+24;
G=6.67384e-11;
mu=mt*G
rt=6.371e+06;
%Orbita di parcheggio
rp=rt+400000
h2=mu*rp
h=sqrt(h2)
vp=h/rp
%Orbita stazionaria
periodo=24*3600;thetap=2*pi/periodo;
rs=(mu/thetap^2)^(1/3)
vs=rs*thetap
%Orbita di Trasferimento
ecc=(rs-rp)/(rs+rp)
h=sqrt(rs*mu*(1-ecc))
h=sqrt(rp*mu*(1+ecc))
v_apogeo=h/rs
v_perigeo=h/rp
Delta_v_perigeo=v_perigeo-vp
Delta_v_apogeo=v_apogeo-vs
%
% Disegna le orbite
%
set(0,"defaultlinelinewidth",2)
ntheta=1000;dtheta=2*pi/ntheta;theta=dtheta/2:dtheta:2*pi;
%
% Orbita bassa
%
rhop=ones(1,1000)*rp;polar(theta,rhop,'b-');hold on
%
% Orbita Geostazionaria
%
rhos=ones(1,1000)*rs; polar(theta,rhos,'r-')
%
% Orbita di trasferimento
%
rhot=(h^2/mu)./(1+ecc*cos(theta));polar(theta,rhot,'bk -')
