%labo9ese3
clear all
close all
clc

%domanda 1
n=7;
n2=6;
n3=5;

d1=9*diag(ones(1,n));
d2=-3*diag(ones(1,n2),1);
d3=-3*diag(ones(1,n2),-1);
d4=diag(ones(1,n3),-2);
d5=diag(ones(1,n3),+2);

A=d1+d2+d3+d4+d5;
b=[7 ;4; 5; 5; 5; 4; 7];

%domanda 2
Adiag=diag(abs(A));
Aout=sum(abs(A),2)-diag(abs(A));

if Adiag>Aout
    disp('la matrice � a dominanza diagonale')
else
    disp('la matrice non � a dominanza diagonale')
end

%domanda 3
if A==A'
    disp('la matrice � simmetrica e definita positiva')
else
    disp('la matrice non � SDP')
end

%domanda 4 algoritmo di gs
%domanda 5
x0=zeros(n,1);
[x,k]=gs(A,b,x0)

%domanda 6
toll=1e-6;
[xj,kj]=jacobi(A,b,x0,toll)

%domanda 7
Din=diag(1./diag(A));
Bj=eye(n)-Din*A;
T=tril(A);
Bgs=eye(n)-inv(T)*A;

rhoj=max(abs(eig(Bj)));
rhogs=max(abs(eig(Bgs)));