a = 0; b = 1;
T = 1; %timespan


f = @(x,t) (-sin(t) + (1/4)*cos(t)).*sin(x'/2);
u_ex = @(x,t) sin(x'/2).*cos(t);

us = @(t) u_ex(a,t);
ud = @(t) u_ex(b,t);
g0 = @(x) u_ex(x,0);

%% 1

deltat = 0.1;
theta = 1;
n = 100;
makePlot = 1;

[U, mesh_x , steps_t]= diff_fin(f,a,b,us,ud,g0,T,n,deltat,theta,makePlot)


%% 2.1

deltat = 0.1;
theta = 0;
n = 100;
makePlot = 1;

[U, mesh_x , steps_t]= diff_fin(f,a,b,us,ud,g0,T,n,deltat,theta,makePlot)


%% 2.2
deltat = 0.001;
theta = 0;
n = 10;
makePlot = 1;

[U, mesh_x , steps_t]= diff_fin(f,a,b,us,ud,g0,T,n,deltat,theta,makePlot)





%% 3

figure

theta = 1;


n = 10;
deltat_vect = [0.1, 0.05, 0.025, 0.0125, 0.00625, 0.003125];
n = 10;
makePlot = 0;

err_vect_BE = [];

for k = [1:length(deltat_vect)]
    
    deltat = deltat_vect(k);
    [U, mesh_x , steps_t]= diff_fin(f,a,b,us,ud,g0,T,n,deltat,theta,makePlot);
    
    U_T = U(:,end);
    
    x = linspace(a,b,length(U_T))

    err = max(abs(u_ex(x,T) - U_T));
    
    err_vect_BE = [err_vect_BE err];  
end


loglog(deltat_vect ,err_vect_BE)
grid on
hold on
xlabel('\Delta t')
ylabel('err')

%

theta = 0.5;


n = 10;
deltat_vect = [0.1, 0.05, 0.025, 0.0125, 0.00625, 0.003125];
n = 10;
makePlot = 0;

err_vect_CN = [];

for k = [1:length(deltat_vect)]
    
    deltat = deltat_vect(k);
    [U, mesh_x , steps_t]= diff_fin(f,a,b,us,ud,g0,T,n,deltat,theta,makePlot);
    
    U_T = U(:,end);
    
    x = linspace(a,b,length(U_T))

    err = max(abs(u_ex(x,T) - U_T));
    
    err_vect_CN = [err_vect_CN err];  
end


loglog(deltat_vect ,err_vect_CN)




%

theta = 0;


n = 10;
deltat_vect = [0.001, 0.0005, 0.00025, 0.000125, 0.00003125];
n = 10;
makePlot = 0;

err_vect_FE = [];

for k = [1:length(deltat_vect)]
    
    deltat = deltat_vect(k);
    [U, mesh_x , steps_t]= diff_fin(f,a,b,us,ud,g0,T,n,deltat,theta,makePlot);
    
    U_T = U(:,end);
    
    x = linspace(a,b,length(U_T))

    err = max(abs(u_ex(x,T) - U_T));
    
    err_vect_FE = [err_vect_FE err];  
end


loglog(deltat_vect,err_vect_FE)

loglog(deltat_vect, deltat_vect.^2, '--k')


legend('BE (\theta = 0), n=10', 'CN (\theta = 0.5), n=10', 'FE (\theta = 0), n=10', '\Delta t^{2}')





