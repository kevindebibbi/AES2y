%% 1.1-2
close all
lambda = -42;
f = @(t,y) lambda*y;
y0 = 2;
t0 = 0;
tf = 1;
y_ex = @(t) y0.*exp(lambda*t);
t = [t0:0.001:tf];
figure
plot(t, y_ex(t))
grid on
hold on

% 1.2
h = 0.05;
[t_h, u_h] = feuler(f,tf,y0,h);
plot(t_h, u_h)
% OSS.: INSTABILE, essendo lambda*h = -2.1 -> fuori da regione di stabilità


df_dy = @(t,y) lambda;
[t_h,u_h,k_vect] = beuler_newton (f, df_dy, tf, y0, h);
plot(t_h, u_h)

legend ('y_{ex}', 'FE_{h=0.05}', 'BE-newton_{h=0.05}')



%% 1.3
figure
h = 0.01;
y_ex = @(t) y0.*exp(lambda*t);
t = [t0:0.001:tf];
plot(t, y_ex(t))
grid on
hold on

[t_h, u_h] = feuler(f,tf,y0,h);
plot(t_h, u_h)

df_dy = @(t,y) lambda;
[t_h,u_h,k_vect] = beuler_newton (f, df_dy, tf, y0, h);
plot(t_h, u_h)


legend ('y_{ex}', 'FE_{h=0.01}', 'BE-newton_{h=0.01}')



%% 1.4
h_vect = [0.05 0.03 0.01];

figure

for i = 1:length(h_vect)
    subplot(1,length(h_vect),i);
    h = h_vect(i);
    z = lambda*h;
    y_ex = @(t) y0.*exp(lambda*t);
    t = [t0:0.001:tf];

    plot(t, y_ex(t))
    grid on
    hold on

    [t_h, u_h] = feuler(f,tf,y0,h);
    plot(t_h, u_h)

    df_dy = @(t,y) lambda;
    [t_h,u_h,k_vect] = beuler_fixedPt(f, tf, y0, h);
    plot(t_h, u_h)

    legend ('y_{ex}', sprintf('FE_{h=%.4d}', h), sprintf('BE-fixedPt_{h=%.4d}', h))
    title(sprintf('h=%.2d -> z=%.2d', h,z))
end




