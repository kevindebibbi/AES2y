%% 2.1-2-3

close all
lambda = -42;
f = @(t,y) lambda*y;
y0 = 2;
t0 = 0;
tf = 1;
y_ex = @(t) y0.*exp(lambda*t);
t = [t0:0.001:tf];
figure
plot(t, y_ex(t))
grid on
hold on

h = 0.02;

[t_h,u_h,k_fp_vect] = Crank_Nicolson(f,tf,y0,h);
plot(t_h, u_h)

legend('y_{ex}','CN-fixedPt')

figure
plot(t_h(2:end), k_fp_vect,'-o')
grid on


%% 2.4
h_vect = 0.02*2.^-[0:4];
e_beuler_fp_vect = [];
e_CN_vect = [];

for k = 1:length(h_vect)
   h = h_vect(k);
   
   % BE_fp
   df_dy = @(t,y) lambda;
   [t_h,u_h,k_vect] = beuler_fixedPt(f, tf, y0, h);
   e_beuler_fp = max(abs(y_ex(t_h) - u_h));
   e_beuler_fp_vect = [e_beuler_fp_vect e_beuler_fp];
   
   % CN
   [t_h,u_h,k_fp_vect] = Crank_Nicolson(f,tf,y0,h);
   e_CN = max(abs(y_ex(t_h) - u_h));
   e_CN_vect = [e_CN_vect e_CN];
   
end

figure
loglog(h_vect, e_beuler_fp_vect,'b')
grid on
hold on
loglog(h_vect, e_CN_vect, 'r');

loglog(h_vect, h_vect, '--b')
loglog(h_vect, h_vect.^2, '--r')

legend('BE_{FP}', 'CN', 'h', 'h^2')

