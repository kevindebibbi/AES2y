function [ L, U, x ] = thomas( A, b )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

n = length(diag(A));

c = diag(A, 1);
a = diag(A);
e = diag(A,-1);

alpha = zeros(n,1);
bet = zeros(n-1,1);
alpha(1) = a(1);
for i = 2:n
    bet(i-1) = e(i-1)/alpha(i-1);
    alpha(i) = a(i) - bet(i-1)*c(i-1);
end

y = zeros(n-1,1);
y(1) = b(1);
for i = 2:n
    y(i) = b(i) - bet(i-1)*y(i-1);
end

x = zeros(n-1,1);
x(n) = y(n)/alpha(n);
for i = n-1:-1:1
    x(i) = (y(i) - c(i)*x(i+1))/alpha(i);
end

L = eye(n) + diag(bet,-1);
U = diag(alpha,0) + diag(c,1);
end

