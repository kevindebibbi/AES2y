function fn = smd_f(t,y) %spring-mass-dumper

[n,m] = size(y);
fn = zeros(n,m);

gamma = 0.1;
omegaSq = 1;
A_0 = 0.5;
omega_f = 0.5;

fn(1) = y(2);
fn(2) = - omegaSq*y(1) - gamma*y(2) + A_0*sin(omega_f*t);
    
    
end
