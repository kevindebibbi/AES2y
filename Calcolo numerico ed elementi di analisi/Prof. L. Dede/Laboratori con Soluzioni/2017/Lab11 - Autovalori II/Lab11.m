
close all

A = [10 -1 1 0;
     1 1 -1 3;
     2 0 2 -1;
     3 0 1 5];

gershcircles(A)

%% OSS.: di Gershgorin non fornisce nessun limite inferiore per l’autovalore di modulo minimo,
% infatti il punto {0, 0} del piano complesso `e compreso nell'intersezione tra SR e SC.
% Si ricava invece qualche informazione sull’autovalore di modulo massimo: dato che non potra'
% essere localizzato esternamente al cerchio riga di centro {10,0} e raggio 2, possiamo dedurre
% che sicuramente |λ|_max ≤ 12.


%% OSS.: Gli autovalori di modulo minimo sono complessi coniugati -> hanno stesso modulo
% -> Non sono valide le ipotesi di invpower -> Infatti inwpower NON CONVERGE

tol = 1e-6;
itermax = 1e+3;
x0 = ones(length(diag(A)), 1);
[mu,x,k] = invpower(A,tol,itermax,x0)

fprintf('invpower.m DOES NOT CONVERGE since there are\n2 minimum absolute value Complex Coniugate eigenvalues\n(which of course have the same abs. val.)\n')



%% OSS.: Gli autovettori di M sono gli stessi di A, che sono gli stessi di A^-1 e di M^-1
% L'algoritmo quindi restituisce direttamente l'autovalore di A piu' vicino ad 

s = 12 + 2*i;
sprintf('Through the analysis of the Gershgorin circles\nwe are sure that the max abs(eigenvalue) is\n the nearest one to: %d + %di', real(s), imag(s))
[lambdaAbsMax,x,k]=invpowershift(A, s, tol,itermax,x0)
%% In realta' sfruttando anche il comando eig(A) vedo che lambdaAbsMax e' reale quindi potevo mettere direttamente s = 12;

s = -1 + 3*i;
sprintf('Through the analysis of the Gershgorin circles\nwe are sure that the min abs(eigenvalue) is\n the nearest one to: %d + %di or %d + %di', real(s), imag(s), real(s'), imag(s'))
[lambdaAbsMin,x,k]=invpowershift(A, s, tol,itermax,x0)
%% OSS.: Se metto s = -1 + 3*i; convergo invece all'altro complesso coniugato 


eigVal = eig(A)





%% ITERAZIONI QR
tol = 1e-10;
itermax = 1e+3;

A = @(alpha) [alpha 2 3 13;
              5 11 10 8;
              9 7 6 12;
              4 14 15 1];
          
D_alpha_30 = qrbasic(A(30), tol, itermax)

D_alpha_minus30 = qrbasic(A(-30), tol, itermax)



