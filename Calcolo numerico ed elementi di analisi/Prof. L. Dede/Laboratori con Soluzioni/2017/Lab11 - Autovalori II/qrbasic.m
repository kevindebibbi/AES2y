function [ D ] = qrbasic( A, tol, itermax )


err = tol + 1;

k = 0;

while (err > tol) & (k < itermax)
    
    k = k+1;
   
    [Q,R] = qr(A);
    
    A = R*Q;
    

    errMatr = tril(A) - diag(diag(A)); %% ATTENZIONE!!! -> diag(diag(A))
    
    err = max(max(abs(errMatr)));
    
    D = diag(A);

end

sprintf('Converges in %d iterations', k)
err

end

