% Consideriamo il caso con dimensione 2 -> il funzionale rappresenta un
% paraboloide in R3

% Autovalori distanti (alto numero di condizionamento = lambda_max/lambda_min) -> le curve di livello sono ellissi molto eccentriche
% Autovalori tutti uguali (cond = 1) -> le curve di livello sono circonferenze


close all

A = [6.8 -2.4;
     -2.4 8.2]
 
 b = [4 8]';
 
 x = linspace(-10, 10, 100);
 y = linspace(-10, 10, 100);
 
[X,Y] = meshgrid(x,y); % NB X = tutte le x della griglia; Y = tutte le y della griglia
 
Phi_1 = 0.5* ( A(1,1)*X.^2 + A(2,2)*Y.^2 + 2*A(1,2)*X.*Y ) - b(1)*X - b(2)*Y;
%Phi2 = 0.5* ( A2(1,1)*X.^2 + A2(2,2)*Y.^2 + 2*A2(1,2)*X.*Y ) - b(1)*X - b(2)*Y;
 
figure 
subplot(1,2,1)
surf(X,Y,Phi_1,'Lines','no');
hold on
contour(X,Y,Phi_1)

[px,py] = gradient(-Phi_1);
quiver(X,Y,px,py,'k');

subplot(1,2,2)
contour(X,Y,Phi_1)




%% PRECONDIZIONATORE

% OSS: Il precondizionatore rende meno eccentriche le curve di livello ->
% velocizzo la convergenza

P = [1.0912 -0.8587;
    -0.8587 1.5921]



%% 1)

n = length(diag(A));

x0 = ones(n,1)
tol = 1e-5;
itermax = n+1;
[x_succ k] = conjgrad( A, b, x0, tol, itermax )



%% 2)


nMin = 2;
nMax = 100;

k_vect = [];
cond_vect = [];
n_vect = linspace(nMin,nMax,nMax-nMin+1);




for n = nMin:nMax
    
    A = 4*eye(n) + diag(ones(n-1,1),1) + diag(ones(n-1,1),-1) + 2*diag(ones(n-2,1),2) + 2*diag(ones(n-2,1),-2);
    b = ones(n,1);

    itermax = 5000;
    x0 = zeros(n,1);
    tol = 1e-6;
    P = tril(A);

    [x, k] = richardson (A, b, P, x0, tol, itermax);
    
    k_vect = [k_vect; k];
    cond_vect = [cond_vect cond(A)];
end

k_vect

figure
subplot(2,2,1)
plot(n_vect, k_vect)
xlabel('n')
ylabel('k')
grid on

subplot(2,2,2)
semilogy(n_vect, k_vect)
xlabel('n')
ylabel('k')
grid on

subplot(2,2,3)
plot(linspace(nMin,nMax,nMax-nMin+1), cond_vect)
xlabel('n')
ylabel('K2(A)')
grid on



