%% EX.1
close all

sigma   = [0.18   0.3   0.5    0.6    0.72  0.75   0.8   0.9   1.0   ];
epsilon = [0.0005 0.001 0.0013 0.0015 0.002 0.0045 0.006 0.007 0.0085];

n = length(sigma)-1;
a = sigma(1); b = sigma(end);
x_axis = [a:0.001:b];

sigma_1 = 0.4;
sigma_2 = 0.65;
data_vect_1 = [];
data_vect_2 = [];

%% Lagrange
subplot(1,2,1)
p = polyfit(sigma, epsilon, n);
pz = polyval(p, x_axis);

data_vect_1 = [data_vect_1; polyval(p,sigma_1)];
data_vect_2 = [data_vect_2; polyval(p,sigma_2)];

plot(x_axis, pz)
grid on

hold on
plot(sigma, epsilon, 'o')

legend('\Pi_{n}', 'Experimental Data')

%% Interp. comp. lin.
subplot(1,2,2)
pz = interp1(sigma, epsilon, x_axis);
plot(x_axis, pz)

data_vect_1 = [data_vect_1; interp1(sigma, epsilon, sigma_1)];
data_vect_2 = [data_vect_2; interp1(sigma, epsilon, sigma_2)];

hold on
grid on

%% Minimi quadrati
d_vect = [1 2 4];


for k = 1:length(d_vect)    
    p = polyfit(sigma, epsilon, d_vect(k));
    pz = polyval(p, x_axis);
    
    data_vect_1 = [data_vect_1; polyval(p,sigma_1)];
    data_vect_2 = [data_vect_2; polyval(p,sigma_2)];

    plot(x_axis, pz)
end


%% Cubic Spline
pz = cubicspline(sigma, epsilon, x_axis);
plot(x_axis, pz)
data_vect_1 = [data_vect_1; cubicspline(sigma, epsilon, sigma_1)];
data_vect_2 = [data_vect_2; cubicspline(sigma, epsilon, sigma_2)];


%% Spline Not-A-Knot
pz = spline(sigma, epsilon, x_axis);
data_vect_1 = [data_vect_1; spline(sigma, epsilon, sigma_1)];
data_vect_2 = [data_vect_2; spline(sigma, epsilon, sigma_2)];
plot(x_axis, pz)

plot(sigma, epsilon, 'o')

set(0,'defaultTextInterpreter','latex')
legend('\Pi_{1}^{H}', 'M.Q._{1}', 'M.Q._{2}', 'M.Q._{4}', 'S_{3}^{(Cubic)}', 'S_{3}^{(N-a-K)}', 'Experimental Data')



data_vect_1
data_vect_2
% Vediamo subito che Lagrange funziona male agli estremi (fenomeno di RUNGE)

