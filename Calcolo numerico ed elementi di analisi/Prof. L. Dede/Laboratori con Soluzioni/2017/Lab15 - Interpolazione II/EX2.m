clear all
close all

%% 2.1
f = @(x) exp(-x.^2).*sin(x);
a = -2; b = 3;
x_axis = [a:0.001:b];
plot(x_axis, f(x_axis))
hold on

n = 3;
H = (b-a)/n;


x = [a:H:b];


pz = interp1(x, f(x), x_axis);
plot(x_axis, pz)
grid on

%% 2.2
e_H = max(abs(f(x_axis)-pz))


%% 2.3

n_vect = 2.^[2 3 4 5 6 7];

e_H_vect = [];
H_vect = [];
figure
for k = 1:length(n_vect)
    
    n = n_vect(k)
    H = (b-a)/n
    x = [a:H:b];

    pz = interp1(x, f(x), x_axis);

    e_H = max(abs(f(x_axis)-pz));
    e_H_vect = [e_H_vect e_H];
    
    H_vect = [H_vect H];
end

loglog(H_vect, e_H_vect)

hold on
quadr = @(x) x.^2;
plot(H_vect, quadr(H_vect))
grid on

% Vediamo che la retta y=H^2 ha la stessa pendenza


