function [ L,U ] = luGauss( A )

n = length(diag(A));

L = eye(n); % L'algoritmo da' per scontato che gli l_kk siano tutti 1

for k = 1:n-1 
    
    for i = k+1:n
        
        l_ik = A(i,k)/A(k,k);
        L(i,k) = l_ik;
        
        if l_ik == 0     
            error('Pivot nullo')
        end
        
        for j = k+1:n
            
            A(i,j) = A(i,j) - l_ik*A(k,j);
            
        end
        
    end
    
    
end


U = triu(A); % Importante: mi azzera tutti gli elementi sotto la diagonale
L = tril(L);
end


