clc
clear

theta=[0:0.01:2*pi];
raggio=1;
x=raggio*cos(theta);
y=raggio*sin(theta);
plot(x,y,'r')
hold on
a=2;
b=3;
x=a*cos(theta);
y=b*sin(theta);
plot(x,y,'g')
plot([2.5 -2.5 -2.5 2.5 2.5],[2.5 2.5 -2.5 -2.5 2.5],'-b')
axis equal % per imporre la stessa dimensione degli assi
legend('Circonferenza','Ellisse','Quadrato')
