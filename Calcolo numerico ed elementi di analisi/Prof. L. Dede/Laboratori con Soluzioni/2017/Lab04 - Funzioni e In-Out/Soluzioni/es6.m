clc
clear

a(1) = sqrt(2);
b(1) = 2;
p(1) = 2*a(1);
q(1) = 2*b(1);
N = 10;       % Numero di elementi delle successioni da calcolare

for n = 2:N
  a(n) = sqrt(2) * sqrt(1 - sqrt(1-1/4*a(n-1)^2));
  b(n) = a(n) / sqrt(1 - 1/4*a(n-1)^2);
  p(n) = 2^n*a(n);
  q(n) = 2^n*b(n);
end

p(end)
q(end)

figure
plot([1:N], p, 'r.-', 'MarkerSize', 20)
hold on
plot([1:N], q, 'b.-', 'MarkerSize', 20)
plot([1:N], pi*ones(1,N), 'k--', 'Linewidth', 1.5)
legend('p_n', 'q_n', 'pi')
