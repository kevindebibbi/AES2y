clc
clear

n=1;
while ( sum([1:n]) < 88 )     
   n=n+1;
end
n

% ----------------------------------- % 

% OPPURE

% clc
% clear
% somma=0;
% n=0;
% while ( somma < 88)
%     n=n+1;
%     somma=somma + n;
% end
% n
