function [A] = es9(N,k)
A=zeros(N,N);
for i=1:N
	for j=1:N
		A(i,j)=2*i*j + (k+1);
	end
end
