close all

A = [2 1; 3 4]
d = det2(A);

sprintf('The matrix determinant is: %d \n', d)
sprintf('The matrix determinant is: %.2f \n', d)
sprintf('The matrix determinant is: %e \n', d)


%% EX. 3

n = 8
v = zeros(1,n);
for k = 1:n+1
    
    v(k) = (2*(k-1) + 1)^2;
    
    
end
v

% alternative method

n = 8;
v = [1];

for k = 1:n
   v = [v ((2*k)+1)^2]; 
end
v

%% EX. 4
% function implementation

v = succEx3(12)



%% EX. 5

n = 26127419;
k = 1;
toll = 1e-6;

incr = toll + eps(toll);

v = [0.5*(v(k)+n/v(k))];

while (incr > toll)
    
    v = [v 0.5*(v(k)+n/v(k))];
    incr = abs(v(k+1) - v(k));
    
    k = k+1;

end
v

figure
plot(v, '-o')
grid on



%% EX. 6
figure
theta = [0:.01:2*pi];
r = 1;
x = r * cos(theta);
y = r * sin(theta);
plot(x,y, 'r')
grid on
hold on

a = 2;
b = 3;
x = a * cos(theta);
y = b * sin(theta);
plot(x,y, 'b')
axis equal



%% PIECEWISE FUNCTION
figure
f = @(x) -sqrt(x.^2-x).*(x<0) + (-x.^2+2.*x).*exp(-x).*(x>=0);
x = linspace(-10, 10, 1000)
plot(x, f(x))
grid on


%% EX. 9
k = 3;
n = 10;
ex09(n,k)



