clear all
close all

n=21;
L=1;
h=L/(n+1);
beta=1; % beta=0; % per il primo punto
x=[0:h:L]';
x_plot=linspace(0,L,1000);
u_ex=@(x) exp(3*x).*(x-x.^2)+1;

% BC DIRICHLET

for epsilon=[1.e-2,1.e-4] % epsilon=1 % per il primo punto

    % f e A vanno ridefinite per ogni valore dei parametri!!!
    f=@(x)exp(3*x).*(beta-4*epsilon+(3*epsilon+beta)*x+3*(3*epsilon-beta)*x.^2);



    %%%  SCHEMA CENTRATO
    A=spdiags([-(beta/(2*h)+epsilon/h^2)*ones(n+2,1),...
               2*epsilon/h^2*ones(n+2,1),...
               (beta/(2*h)-epsilon/h^2)*ones(n+2,1)],...
               -1:1,n+2,n+2);

    A(1,:)=[1,zeros(1,n+1)];
    A(end,:)=[zeros(1,n+1),1];

    b=[1;f(x(2:end-1));1];

    u=A\b;

    % % oppure con thomas
    % 
    % c=[0,(+beta/(2*h)-epsilon/h^2)*ones(1,n)];
    % e=[-(beta/(2*h)+epsilon/h^2)*ones(1,n),0];
    % a=[1,2*epsilon/h^2*ones(1,n),1];
    % 
    % [~,~,u]=thomas(c,e,a,b);

    % figure;
    % plot(x,u,'r-o',x_plot,u_ex(x_plot));
    % title(['DF centrate, \epsilon=',num2str(epsilon)])

    %%%  UPWIND
    A_up=spdiags([-(beta/h+epsilon/h^2)*ones(n+2,1),...
               (2*epsilon/h^2+beta/h)*ones(n+2,1),...
               -epsilon/h^2*ones(n+2,1)],...
               -1:1,n+2,n+2);

    A_up(1,:)=[1,zeros(1,n+1)];
    A_up(end,:)=[zeros(1,n+1),1];

    b=[1;f(x(2:end-1));1];


    u_up=A_up\b;

    figure;
    plot(x,u_up,'g-*',x,u,'r-o',x_plot,u_ex(x_plot));
    title(['\epsilon=',num2str(epsilon),' BC Dirichlet'])
    legend('upwind','DF centrate','u_{ex}','location','NorthWest');
end

%%% BC MISTE

for epsilon=[1.e-2,1.e-4]

f=@(x)exp(3*x).*(beta-4*epsilon+(3*epsilon+beta)*x+3*(3*epsilon-beta)*x.^2);

        %%% SCHEMA CENTRATO
        A=spdiags([-(beta/(2*h)+epsilon/h^2)*ones(n+2,1),...
               2*epsilon/h^2*ones(n+2,1),...
               (beta/(2*h)-epsilon/h^2)*ones(n+2,1)],...
               -1:1,n+2,n+2);

        A(1,:)=[1,zeros(1,n+1)];
        A(end,:)=[zeros(1,n),-1/h,1/h];

        b=[1;f(x(2:end-1));-exp(3)];

        u=A\b;
        % figure;
        % plot(x,u,'r-o',x_plot,u_ex(x_plot));
        % title(['DF centrate, \epsilon=',num2str(epsilon),' BC miste'])

        %%% UPWIND
        A_up=spdiags([-(beta/h+epsilon/h^2)*ones(n+2,1),...
               (2*epsilon/h^2+beta/h)*ones(n+2,1),...
               -epsilon/h^2*ones(n+2,1)],...
               -1:1,n+2,n+2);

        A_up(1,:)=[1,zeros(1,n+1)];

        A_up(end,:)=[zeros(1,n),-1/h,1/h];

        b=[1;f(x(2:end-1));-exp(3)];

        u_up=A_up\b;
        figure;
        plot(x,u_up,'g-*',x,u,'r-o',x_plot,u_ex(x_plot));
        title(['\epsilon=',num2str(epsilon),' BC miste'])
        legend('upwind','DF centrate','u_{ex}','location','NorthWest');

end

