% Esercizio 1
clear all
clc
close all

fprintf( '######## Esercizio 1 ########\n')

%%% PUNTO 1 %%%

ts = 0:0.1:1;
ys = [10.0 9.92 9.79 9.55 9.23 8.77 8.20 7.52 6.80 6.01 5.10 ];

% Legge ideale
y0 = 10;
g = 9.81 ;
yid = @(t,y0,g) y0-0.5*g*t.^2;

t = linspace(min(ts) ,max(ts),1000);
yid_t= yid ( t , y0 , g ) ;
figure(1);
axes ( 'FontSize' ,12)
plot ( ts , ys , 'mo' , t, yid_t, 'k-' , 'linewidth' , 2)
title ( ' Dati sperimentali & legge di moto ideale ' )
xlabel ( 'Tempo [s] ' )
ylabel ( ' Posizione [m] ' )
legend ( ' Dati Sper. ' , ' Legge Ideale ' ) ;

fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 2 %%%

% Interpolazione di Lagrange ( IL )
grado = length(ts)-1;
PL = polyfit( ts , ys , grado ) ;
yIL= polyval(PL, t) ;
% Interpolazione composita lineare ( ICL )
yICL = interp1( ts , ys , t ) ;
% Interpolazione ai minimi quadrati (IMQ)
PMQ = polyfit( ts , ys , 2 ) ;
yIMQ = polyval(PMQ, t ) ;
% Interpolazione con spline cubica naturale (ISCN )
yISCN = cubicspline(ts,ys,t) ;

figure(2)
axes ( 'FontSize' ,12)
plot (ts,ys,'mo', t,yid_t,'k-', t,yIL, t,yICL, t,yIMQ, t,yISCN);
title ( ' Interpolazioni a confronto con la legge di moto ideale ' )
xlabel ( 'Tempo [s] ' )
ylabel ( ' Posizione [m] ' )
legend ( ' Dati Sper. ' , ' Legge Ideale ' , ' IL ' , ' ICL ' , 'IMQ 2 ' , 'ISCN ' ) ;


% Calcolo degli errori
errIL = abs ( yid_t - yIL ) ;
errICL = abs ( yid_t - yICL ) ;
errIMQ = abs ( yid_t - yIMQ ) ;
errISCN = abs ( yid_t - yISCN ) ;

figure(3)
axes ( 'FontSize' ,12)
plot ( t,errIL,t,errICL,t,errIMQ,t,errISCN, 'linewidth' , 2)
title ( ' Errori delle interpolazioni a confronto ' )
xlabel ( 'Tempo [s] ' )
ylabel ( ' Errore ' )
legend ( ' IL ' , ' ICL ' , 'IMQ 2 ' , 'ISCN ' ) ;

fprintf ( '\nErrori commessi in norma infinito :\n' )
fprintf ( ' IL\t: %f \n' , max(errIL) );
fprintf ( ' ICL\t: %f \n' , max(errICL) );
fprintf ( ' IMQ\t: %f \n' , max(errIMQ) );
fprintf ( ' ISCN\t: %f \n' , max(errISCN) );


fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 3 %%%

tout = 1.05 ;
fprintf ( '\nValori della posizione per t = 1.05 :\n' )
fprintf ( ' Esatto\t: y = %f \n' , yid ( tout , y0 , g ) )
fprintf ( ' IL\t: y = %f \n' , polyval (PL, tout) )
fprintf ( ' IMQ\t: y = %f \n' , polyval (PMQ, tout) )


tnew= linspace ( min (ts) , 1.1 , 1000);
yid_tnew= yid ( tnew , y0 , g ) ;
yIL = polyval (PL , tnew) ;
yIMQ = polyval (PMQ, tnew) ;

figure(4)
axes ( 'FontSize' ,12)
plot (ts,ys,'mo',tnew,yid_tnew,'k-',tnew,yIL,'b',tnew,yIMQ,'r',...
    'linewidth',2)
xlim ( [ 0 1.1 ] )
title ( ' Confronto delle estrapolazioni' )
xlabel ( 'Tempo [s] ' )
ylabel ( ' Posizione [m] ' )
legend ( ' Dati Sper. ' , ' Legge Ideale ' , ' IL ' , 'IMQ 2 ' , 2 ) ;

