% questo comando serve per includere anche la cartella Fornite nell'insieme
% delle cartelle tra le quali matlab cerca le functions. (Di default cerca 
% solo nella cartella corrente)
addpath ../Fornite

%%%% ES 1 %%%%
close all
clear all
% definizione della funzione e delle sue derivate
f=@(x) 10*(1-exp(sin(x)-1)).*(x-1);
fx=@(x) 10*((-cos(x).*exp(sin(x)-1)).*(x-1)+(1-exp(sin(x)-1)));
fxx=@(x) 10*((sin(x)-(cos(x)).^2).*exp(sin(x)-1).*(x-1)+2*(-cos(x).*exp(sin(x)-1)));
% plot della funzione e localizzazione degli zeri
x=linspace(0.8,2,100);
plot(x,f(x));
grid on
x0=1.8;
% definizione della phi per il metodo di punto fisso e della sua derivata
phi=@(x) x-f(x)./fx(x);
phix=@(x) 1-(fx(x).^2-f(x).*fxx(x))./(fx(x).^2);
nmax=100;
toll=1.e-6;
a=0.8;
b=2;

% calcolo della prima radice
figure;
x0=1.1;
fprintf('\n\t_____PRIMA RADICE (semplice)_____\n')
[succ,it] = ptofis(x0,phi,nmax,toll,a,b);
fprintf([' Fattore di riduzione teorico : ',num2str(abs(fxx(succ(end))/(2*fx(succ(end))))),'\n']);
[p,c]=stimap(succ);

% calcolo della seconda radice
figure;
x0=1.8;
fprintf('\n\t_____SECONDA RADICE (molteplicità 2)_____\n')
[succ,it] = ptofis(x0,phi,nmax,toll,a,b);
fprintf([' Fattore di riduzione teorico : ',num2str(abs(phix(succ(end)))),'\n']);
[p,c]=stimap(succ);
