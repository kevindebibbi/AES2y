B = [10 -1 1 0; 1 1 -1 3; 2 0 2 -1; 3 0 1 5]
gershcircles(B)
close all
gershcircles_mod(B)

eig(B)
[lambda,x,iter]=invpower(B,1e-6,1000,ones(4,1)); % non converge
% shift +i , -i
[lambda,x,iter]=invpowershift(B,i,1e-6,1000,ones(4,1));
[lambda,x,iter]=invpowershift(B,-i,1e-6,1000,ones(4,1));

% lambda max, parto dal bordo dei cerchi di gersh
[lambda,x,iter]=invpowershift(B,12,1e-6,1000,ones(4,1));   % niter = 12
% confronto con potenze
[lambda,x,iter]=eigpower(B,1e-6,1000,ones(4,1));            % niter = 20


%% Esercizio 2

A1 = [30 2 3 13; 5 11 10 8; 9 7 6 12 ; 4 14 15 1];
D1 = qrbasic(A1, 1e-10, 1000)  % 41 iterazioni
A2 = [-30 2 3 13; 5 11 10 8; 9 7 6 12 ; 4 14 15 1];
D2 = qrbasic(A2, 1e-10, 1000)  % 843 iterazioni

for i=1:length(D1)-1
    v1(i) = abs(D1(i+1)/D1(i));
end
V1 = max(v1)

for i=1:length(D2)-1
    v2(i) = abs(D2(i+1)/D2(i));
end
V2 = max(v2)