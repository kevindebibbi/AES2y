function fn = mms_forz(t,y)
gamma = 0.1; 
omega2 = 1;
A = 0.5;
w0 = 0.5;
[n,m] = size(y);
fn = zeros(n,m);
fn(1) = y(2);
fn(2) = -gamma*y(2) - omega2*y(1) - A*sin(w0*t);
return