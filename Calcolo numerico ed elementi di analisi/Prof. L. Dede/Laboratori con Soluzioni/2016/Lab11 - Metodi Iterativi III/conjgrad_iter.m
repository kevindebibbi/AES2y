function [x,i] = conjgrad(A,b,x0,nmax,toll)
  bnrm2 = norm(b);
  xv = x0;
  r = b - A*x0;
  p = r;
  x=x0
  
  %questa implementazione è leggermente diversa dalle precedenti: al posto di usare un while si usa un for, specificando di uscire
  %anticipatamente dal ciclo se si soddisfa il criterio di arresto. Questo si ottiene tramite il comando break, che fa uscire dal ciclo for
  %che lo contiene. NB: in caso di cicli iterativi annidati, break esce solo dal ciclo più interno.
  
  for i = 1:nmax
    %q = A*p;
    alpha = (p'*r)/(p'*A*p);
    xn = xv + alpha * p;
    x=[x, xn];
    xv=xn;
    %rho = r'*r;
    r = r - alpha * A*p;
    if norm(r)/bnrm2 < toll 
      break
    end
    beta = (p'*A*r)/(p'*A*p);
    p = r - beta * p; 
  end
