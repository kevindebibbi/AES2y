A = gallery(3);
n = length(diag(A));
x0 = ones(n,1);
tol = 1e-6;
itermax = 100;

lambdaMax = eigpower( A, x0, tol, itermax)