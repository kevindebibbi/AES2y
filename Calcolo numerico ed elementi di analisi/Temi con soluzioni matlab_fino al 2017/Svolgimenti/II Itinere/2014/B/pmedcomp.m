function [ I ] = pmedcomp( a,b,f, M )



H = (b-a)/M;

x_m = [(a+H/2):H:(b-H/2)];

I = H * sum (f(x_m));  

end

