close all
clear all

x0 = 0;
xf = 1;
sigma = @(x) (pi^2)*sin(pi*x);

u_ex = @(x) exp(sin(pi*x));
du_ex = @(x) pi*cos(pi*x).*exp(sin(pi*x));

h = 0.2;

N = (xf-x0)/h -1;
alpha = u_ex(x0);
beta = du_ex(xf);

knots_int = [x0+h:h:xf-h];


A = (1/(h^2))*(diag(-ones(N-1,1),-1) + 2*diag(-ones(N,1),0) + diag(-ones(N-1,1),1));
A(N,N) = 1/(h^2);
A = A + sigma(diag(knots_int));

b = A*u_ex(knots_int');

f = b;
f(1) = f(1) - alpha/(h^2);
f(end) = f(end) - beta/h;
f

u_h = A\b;
u_h = [alpha; u_h; u_h(end) + h*beta]

x = [x0; knots_int';
    xf]

figure
plot(x, u_h)
grid on
hold on
x_axis = [x0:0.001:xf];
plot(x_axis, u_ex(x_axis))

legend('u_h', 'u_ex')















