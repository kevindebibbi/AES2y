close all
clear all

set(0,'defaultTextInterpreter','latex')

lambda = -20;

f = @(t,y) lambda * (y-cos(t));
y0 = 0;
t0 = 0;
tf = 20;

y_ex = @(t) -lambda * (lambda*exp(lambda*t) - lambda*cos(t) + sin(t)) / (lambda^2+1);

t_axis = [t0:0.001:tf];
plot(t_axis, y_ex(t_axis))
grid on
hold on

h_vect = 0.02*2.^-[0:4];



tolFP = 1e-5;
itermaxFP = 1000;

e_h_vect = [];


for i = 1:length(h_vect)
    h = h_vect(i);
    [t_h,u_h,itFP_vect] = Crank_Nicolson(f,t0,tf, y0, h, tolFP, itermaxFP);
    plot(t_h, u_h)
    
    e_h = max(abs(y_ex(t_h) - u_h));
    e_h_vect = [e_h_vect e_h];
    
end



legend('y_{ex}(t)', 'u_{n}^{(h=0.02)}', 'u_{n}^{(h=0.01)}', 'u_{n}^{(h=0.005)}', 'u_{n}^{(h=0.0025)}', 'u_{n}^{(h=0.00125)}')
xlabel('$t$ (time)')



axes('position',[.65 .175 .2 .25])
box on
for i = 1:length(h_vect)
    h = h_vect(i);
    [t_h,u_h,itFP_vect] = Crank_Nicolson(f,t0,tf, y0, h, tolFP, itermaxFP);
    t_rangeOfInterest = (t_h > 5.0) & (t_h < 5.12); % range of t
    t_h_zoom = t_h(t_rangeOfInterest);
    u_h_zoom = u_h(round((t_h_zoom(1)-t0)/h):round((t_h_zoom(end)-t0)/h));
    plot(t_h_zoom, u_h_zoom) % plot on new axes
    hold on
    grid on
    axis tight
    title('Zoomed Area')
end




figure
loglog(h_vect, e_h_vect);
grid on
hold on
loglog(h_vect, h_vect.^2, '--k');
legend('e_{h}', 'h^{2}')
xlabel('h (discretization step)')
ylabel('$e_{h}$ (error)')


%% 
p_appr = log(e_h_vect(1)/e_h_vect(2)) / log(h_vect(1)/h_vect(2))
p_appr = log(e_h_vect(2)/e_h_vect(3)) / log(h_vect(2)/h_vect(3))
p_appr = log(e_h_vect(3)/e_h_vect(4)) / log(h_vect(3)/h_vect(4))