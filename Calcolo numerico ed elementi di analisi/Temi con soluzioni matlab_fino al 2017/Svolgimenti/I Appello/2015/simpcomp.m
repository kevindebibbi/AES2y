function [ I ] = simpcomp(a, b, M, f)

H = (b-a)/M;


I = 0;


x = [a:H:b];
x_m = [a+H/2:H:b-H/2];

for k = 1:M
    
    I = I + (H/6) * (f(x(k)) + 4*f(x_m(k)) + f(x(k+1)));
    
end


end

