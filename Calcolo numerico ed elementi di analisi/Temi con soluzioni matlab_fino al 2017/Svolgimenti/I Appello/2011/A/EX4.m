t0 = 0;
tf = 20;
y0 = [10 2];

[t,y] = ode45(@lotkaEX2011, [t0 tf], y0)

plot(t,y(:,1))
grid on
hold on
plot(t,y(:,2))

legend('Preys', 'Predators')