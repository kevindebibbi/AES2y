function [x_vect,k] = newton(x0,itermax,tol,fun,dfun,m)

%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

if nargin == 5
   m = 1; %% Se non ? detto nulla -> Newton non modificato
end


x = x0;

k = 0;

x_vect = [x];

err = tol + 1;

while (k < itermax) && (err > tol)
   
    k = k+1;
    
    x_old = x;
    
    if dfun(x) == 0
    error('Derivata prima nulla')
    return
  
    else
    x = x - m * fun(x)/dfun(x);
    end
    
    err = abs(x-x_old);    
    x_vect = [x_vect; x];
    
end

end

