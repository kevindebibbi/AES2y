function [ x_vect, k] = quasiNChord( f,a,b,x0,tol,itermax )

k = 0;
x = x0;
x_vect = [];
err = tol + 1;

while (k < itermax) && (err > tol)
    k = k+1;
    
    q = (f(b)-f(a))/(b-a);
    x_old = x;
    x = x - f(x)/q;
    x_vect = [x_vect; x];
    
    err = abs(x-x_old);
end


return

