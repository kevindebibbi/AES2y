close all
%% 1.1
n = 20;
A = 30*eye(n) + diag([1:n-1],-1) + diag([1:n-1],1) + 0.1*diag([3:n],-2) + 0.1*diag([3:n],2)
x_exact = ones(n,1);
b = A*x_exact %Ax=b -> b = A*x;

%% 1.1.a
[L,U,P] = lu(A)

%% 1.1.b
%Ly=b
y = fwsub(L,P*b);
%Ux=y
x = bksub(U,y)

%% 1.1.c
r = b - A*x;
r_rel = norm(r)/norm(b)

e_rel = norm(x-x_exact)/norm(x_exact)


%% 1.1.d
%(theory)


%% 1.2.a
C = [1 2 3; 2 4 7; 3 5 3]
[L,U,P] = lu(C)

% C1 = P*C
% [L1,U1,P1] = lu(C1)



%% 2.1
f = @(x) sinh(x);
x = [-1:0.001:1];
plot(x,f(x))
hold on
plot(x,zeros(length(x),1))
grid on

c = 2
phi = @(x) x-exp(c*x).*sinh(x);

itermax = 100;
tol = 1e-9;
x0 = 0.8;

[succ, k] = fixedPt(x0, phi, itermax, tol);
k
[~,~] = stimap(succ)


%% 2.4.b
c = 0
phi = @(x) x-exp(c*x).*sinh(x);
[succ, k] = fixedPt(x0, phi, itermax, tol);
k
[~,~] = stimap(succ)


