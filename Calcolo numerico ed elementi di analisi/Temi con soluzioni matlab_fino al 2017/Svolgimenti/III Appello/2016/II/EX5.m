clear all
close all

x0 = 0;
xf = pi/2;
mu = 1;
etha = 1;
sigma = -1;
f = @(x) cos(x);
u_ex = @(x) sin(x);


%% 2-3

x_axis = [x0:0.001:xf];
plot(x_axis,u_ex(x_axis))
grid on
hold on


N_vect = 10.^[1:3];
h_vect = (xf-x0)./(N_vect+1);
e_h_vect = [];
for i = 1:length(N_vect)
    N = N_vect(i);
    h = h_vect(i);
    knots_int = [x0+h:h:xf-h]';
    
    A = (mu/(h^2))*( diag(-ones(N-1,1),-1) + diag(2*ones(N,1),0) + diag(-ones(N-1,1),1) ) + (etha/(2*h))*( diag(-ones(N-1,1),-1) + diag(ones(N-1,1),1) ) + sigma*eye(N);
    
    alpha = u_ex(x0);
    beta = u_ex(xf);
    
    
    b = f(knots_int);
    b(1) = b(1) + ((mu/(h^2)) + (etha/(2*h))) * alpha;
    b(end) = b(end) + ((mu/(h^2)) - (etha/(2*h))) * beta;
    
    
    [ ~, ~, u_h ] = thomas( A, b );
    x = [x0; knots_int; xf];
    u_h = [alpha; u_h; beta];
    
    plot(x,u_h)
    
    
    e_h = max(abs(u_ex(x) - u_h));
    e_h_vect = [e_h_vect e_h];
    
end

figure
loglog(h_vect, e_h_vect);
grid on
hold on
loglog(h_vect, h_vect.^2, '--k');
legend('e_{h}', 'h^{2}')

