close all

s = tf('s');
G1 = 80/(s^2+0.8*s+16);
G2 = 80/(s^2+7.2*s+16);
G3 = 80/(s^2+16);

%%
figure
subplot(1,3,1)
bode(G1)
grid on

subplot(1,3,2)
bode(G2)
grid on

subplot(1,3,3)
bode(G3)
grid on


%%
figure
subplot(1,3,1)
nyquist(G1)
grid on

subplot(1,3,2)
nyquist(G2)
grid on

subplot(1,3,3)
nyquist(G3)
grid on


%%
figure
subplot(1,3,1)
step(G1)
grid on

subplot(1,3,2)
step(G2)
grid on

subplot(1,3,3)
step(G3)
grid on